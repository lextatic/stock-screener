﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StockScreener
{
	public partial class StockScreener : Form
	{
		private List<Company> CompanyList { get; set; }
		private List<Indicator> IndicatorsList { get; set; }

		public StockScreener()
		{
			InitList();
			InitializeComponent();
		}

		private void InitList()
		{
			IndicatorsList = new List<Indicator>();
			IndicatorsList.Add(new Indicator()
			{
				Name = "Ticker",
				IndicatorType = IndicatorType.Outros,
				ValueCalculator = new DummyValueCalculator()
			});

			#region All Rounders
			IndicatorsList.Add(new Indicator()
			{
				Name = "P/LDL",
				IndicatorType = IndicatorType.AllRounders,
				FormatType = FormatType.TwoDecimalPlaces,
				OrderBy = Ordering.Descending,
				MinValue = 0,
				MaxValue = 50,
				ValueCalculator = new RegexExtractValueCalculator(
					Indicator.RegexIndicatorsPart1 +
					Indicator.RegexPL +
					Indicator.RegexFloatIndicatorsPart3_2,
					2)
			});

			IndicatorsList.Add(new Indicator()
			{
				Name = "P/VP ↓",
				IndicatorType = IndicatorType.AllRounders,
				FormatType = FormatType.TwoDecimalPlaces,
				OrderBy = Ordering.Descending,
				MinValue = 0,
				MaxValue = 4,
				ValueCalculator = new RegexExtractValueCalculator(
					Indicator.RegexIndicatorsPart1 +
					Indicator.RegexPVP +
					Indicator.RegexFloatIndicatorsPart3_2,
					2)
			});
			#endregion

			#region Lucratividade
			IndicatorsList.Add(new Indicator()
			{
				Name = "ROE ↑",
				IndicatorType = IndicatorType.Lucratividade,
				FormatType = FormatType.Percentage,
				OrderBy = Ordering.Ascending,
				MinValue = 0,
				MaxValue = 50,
				ValueCalculator = new RegexExtractValueCalculator(
					Indicator.RegexIndicatorsPart1 +
					Indicator.RegexROE +
					Indicator.RegexFloatIndicatorsPart3_2,
					2)
			});

			IndicatorsList.Add(new Indicator()
			{
				Name = "Margem Líquida ↑",
				IndicatorType = IndicatorType.Lucratividade,
				FormatType = FormatType.Percentage,
				OrderBy = Ordering.Ascending,
				MinValue = 0,
				ValueCalculator = new RegexExtractValueCalculator(
					Indicator.RegexIndicatorsPart1 +
					Indicator.RegexMLiquida +
					Indicator.RegexFloatIndicatorsPart3_2,
					2)
			});
			#endregion

			#region Crescimento
			IndicatorsList.Add(new Indicator()
			{
				Name = "CAGR Lucro 5 Anos ↑",
				IndicatorType = IndicatorType.Crescimento,
				FormatType = FormatType.Percentage,
				OrderBy = Ordering.Ascending,
				ValueCalculator = new RegexExtractValueCalculator(
					Indicator.RegexIndicatorsPart1 +
					Indicator.RegexCAGRLucro5a +
					Indicator.RegexFloatIndicatorsPart3_2,
					2)
			});

			IndicatorsList.Add(new Indicator()
			{
				Name = "PEG Ratio ↓",
				IndicatorType = IndicatorType.Crescimento,
				FormatType = FormatType.TwoDecimalPlaces,
				OrderBy = Ordering.Descending,
				MinValue = 0,
				ValueCalculator = new DivisionValueCalculator(0, 4, 1, 1)
			});
			#endregion

			#region Endividamento
			IndicatorsList.Add(new Indicator()
			{
				Name = "DL/EBIT ↓",
				IndicatorType = IndicatorType.Endividamento,
				FormatType = FormatType.TwoDecimalPlaces,
				OrderBy = Ordering.Descending,
				MaxValue = 4,
				ValueCalculator = new RegexExtractValueCalculator(
					Indicator.RegexIndicatorsPart1 +
					Indicator.RegexDivLiqEbit +
					Indicator.RegexFloatIndicatorsPart3_2,
					2)
			});
			#endregion

			#region Governança
			IndicatorsList.Add(new Indicator()
			{
				Name = "Recompra",
				IndicatorType = IndicatorType.Governanca,
				FormatType = FormatType.Ativo,
				ValueCalculator = new RegexNonValueCalculator(
					Indicator.RegexRecompra,
					2)
			});
			#endregion

			#region Outros
			IndicatorsList.Add(new Indicator()
			{
				Name = "Valor de Mercado",
				IndicatorType = IndicatorType.Outros,
				FormatType = FormatType.B,
				ValueCalculator = new RegexExtractValueCalculator(
					Indicator.RegexIndicatorsPart1 +
					Indicator.RegexValorMercadoPart2 +
					Indicator.RegexValorMercadoPart3,
					3,
					0.000001f)
			});

			//IndicatorsList.Add(new Indicator()
			//{
			//	Name = "Fluxo de Caixa Livre",
			//	IndicatorType = IndicatorType.Outros,
			//	FormatType = FormatType.M,
			//	ValueCalculator = new RegexExtractValueCalculator(
			//		Indicator.RegexIndicatorsPart1 +
			//		"Saldo Final" +
			//		Indicator.regexTableIndicatorsPart3,
			//		3)
			//});

			//IndicatorsList.Add(new Indicator()
			//{
			//	Name = "EBITDA",
			//	IndicatorType = IndicatorType.Outros,
			//	FormatType = FormatType.M,
			//	ValueCalculator = new RegexExtractValueCalculator(
			//		Indicator.RegexIndicatorsPart1 +
			//		Indicator.RegexEBITDA +
			//		Indicator.regexTableIndicatorsPart3,
			//		3)
			//});

			//IndicatorsList.Add(new Indicator()
			//{
			//	Name = "Receita Líquida",
			//	IndicatorType = IndicatorType.Outros,
			//	FormatType = FormatType.M,
			//	ValueCalculator = new RegexExtractValueCalculator(
			//		Indicator.RegexIndicatorsPart1 +
			//		Indicator.RegexReceitaLiq +
			//		Indicator.regexTableIndicatorsPart3,
			//		3)
			//});

			//IndicatorsList.Add(new Indicator()
			//{
			//	Name = "Dívida Bruta",
			//	IndicatorType = IndicatorType.Outros,
			//	FormatType = FormatType.M,
			//	ValueCalculator = new RegexExtractValueCalculator(
			//		Indicator.RegexIndicatorsPart1 +
			//		Indicator.RegexDivBruta +
			//		Indicator.regexTableIndicatorsPart3,
			//		3)
			//});
			#endregion


			CompanyList = new List<Company>();
			CompanyList.Add(new Company() { Name = "FLRY3", Values = new float[IndicatorsList.Count - 1] });
			CompanyList.Add(new Company() { Name = "PARD3", Values = new float[IndicatorsList.Count - 1] });
			CompanyList.Add(new Company() { Name = "HAPV3", Values = new float[IndicatorsList.Count - 1] });
			CompanyList.Add(GetNewCompany());
		}

		private void Form1_LoadAsync(object sender, EventArgs e)
		{
			_ = LoadInitialData();
		}

		private async void dataGridView1_CellValueChangedAsync(object sender, DataGridViewCellEventArgs e)
		{
			var company = CompanyList[e.ColumnIndex - 1];

			company.Name = dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString();

			await LoadCompanyData(company, e.ColumnIndex - 1);

			CreateTable();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			CompanyList.Clear();
			CompanyList.Add(GetNewCompany());
			CreateTable();
		}

		private async Task LoadInitialData()
		{
			for (int i = 0; i < CompanyList.Count - 1; i++)
			{
				await LoadCompanyData(CompanyList[i], i);
			}

			CreateTable();
		}

		private async Task LoadCompanyData(Company company, int index)
		{
			string baseURL = $"https://statusinvest.com.br/acoes/{company.Name}";

			try
			{
				using (HttpClient client = new HttpClient())
				{
					using (HttpResponseMessage res = await client.GetAsync(baseURL))
					{
						using (HttpContent content = res.Content)
						{
							string data = await content.ReadAsStringAsync();

							if (data != null)
							{
								company.URLData = data.ToString();

								for (int i = 1; i < IndicatorsList.Count; i++)
								{
									company.Values[i - 1] = IndicatorsList[i].CalculateValue(company);
								}

								CompanyList[index] = company;

								// If it's the last one, create a new one
								if (index == CompanyList.Count - 1)
								{
									CompanyList.Add(GetNewCompany());
								}
							}
							else
							{
								Console.WriteLine("Data is null!");
							}
						}
					}
				}
			}
			catch (Exception exception)
			{
				Console.WriteLine(exception);
			}
		}

		private DataSet FlipDataSet(DataSet my_DataSet)
		{
			DataSet ds = new DataSet();

			foreach (DataTable dt in my_DataSet.Tables)
			{
				DataTable table = new DataTable();
				table.TableName = dt.TableName;

				table.Columns.Add(" ");

				for (int i = 1; i <= dt.Rows.Count; i++)
				{
					table.Columns.Add(i.ToString());
				}

				DataRow r;
				for (int k = 0; k < dt.Columns.Count; k++)
				{
					r = table.NewRow();
					r[0] = dt.Columns[k].ToString();
					for (int j = 1; j <= dt.Rows.Count; j++)
					{
						r[j] = dt.Rows[j - 1][k];
					}
					table.Rows.Add(r);
				}
				ds.Tables.Add(table);
			}

			return ds;
		}

		private DataTable GetCompanies(List<Company> companies)
		{
			DataTable table = new DataTable();
			table.TableName = "Companies";

			foreach (var indicator in IndicatorsList)
			{
				table.Columns.Add(indicator.Name, typeof(string));
			}

			foreach (Company company in companies)
			{
				AddCompanyToDataTable(table.Rows, company);
			}

			table.AcceptChanges();

			return table;
		}

		private void AddCompanyToDataTable(DataRowCollection row, Company company)
		{
			if (company.Name == null)
			{
				row.Add(new object[] { });
				return;
			}

			object[] companyIndicators = new object[IndicatorsList.Count];

			companyIndicators[0] = company.Name.ToUpperInvariant();

			for (int i = 1; i < IndicatorsList.Count; i++)
			{
				switch (IndicatorsList[i].FormatType)
				{
					case FormatType.None:
						companyIndicators[i] = company.Values[i - 1];
						break;

					case FormatType.TwoDecimalPlaces:
						if (float.IsNaN(company.Values[i - 1]))
						{
							companyIndicators[i] = $"-";
						}
						else
						{
							companyIndicators[i] = $"{company.Values[i - 1]:#,#0.#0}";
						}
						break;

					case FormatType.Percentage:
						if (float.IsNaN(company.Values[i - 1]))
						{
							companyIndicators[i] = $"-";
						}
						else
						{
							companyIndicators[i] = $"{(company.Values[i - 1] / 100):P2}";
						}
						break;

					case FormatType.M:
						if (float.IsNaN(company.Values[i - 1]))
						{
							companyIndicators[i] = $"-";
						}
						else
						{
							companyIndicators[i] = $"{company.Values[i - 1]:#,#0.#0 M}";
						}
						break;

					case FormatType.B:
						if (float.IsNaN(company.Values[i - 1]))
						{
							companyIndicators[i] = $"-";
						}
						else
						{
							companyIndicators[i] = $"{company.Values[i - 1] / 1000:#,#0.##0 B}";
						}
						break;

					case FormatType.Ativo:
						if (company.Values[i - 1] == 0)
						{
							companyIndicators[i] = $"-";
						}
						else
						{
							companyIndicators[i] = $"Ativo";
						}
						break;
				}

			}

			row.Add(companyIndicators);
		}

		private void CreateTable()
		{
			DataSet dataSet = new DataSet();
			dataSet.Tables.Add(GetCompanies(CompanyList));
			DataSet transposedDS = FlipDataSet(dataSet);

			dataGridView.DataSource = transposedDS.Tables["Companies"].DefaultView;

			for (int r = 0; r < dataGridView.Rows.Count; r++)
			{
				dataGridView.Rows[r].Resizable = DataGridViewTriState.False;

				for (int c = 0; c < dataGridView.Rows[r].Cells.Count; c++)
				{
					if (r == 0 && c != 0 || r >= IndicatorsList.Count) continue;

					dataGridView.Rows[r].Cells[c].ReadOnly = true;

					if (c == 0)
					{
						switch (IndicatorsList[r].IndicatorType)
						{
							case IndicatorType.AllRounders:
								dataGridView.Rows[r].Cells[c].Style.BackColor = Color.Gold;
								break;

							case IndicatorType.Lucratividade:
								dataGridView.Rows[r].Cells[c].Style.BackColor = Color.LightGreen;
								break;

							case IndicatorType.Crescimento:
								dataGridView.Rows[r].Cells[c].Style.BackColor = Color.LightSkyBlue;
								break;

							case IndicatorType.Endividamento:
								dataGridView.Rows[r].Cells[c].Style.BackColor = Color.MistyRose;
								break;

							case IndicatorType.Governanca:
								dataGridView.Rows[r].Cells[c].Style.BackColor = Color.Thistle;
								break;

							case IndicatorType.Outros:
								dataGridView.Rows[r].Cells[c].Style.BackColor = Color.WhiteSmoke;
								break;
						}
					}
					else
					{
						dataGridView.Rows[r].Cells[c].Style.BackColor = Color.AliceBlue;
						dataGridView.Rows[r].Cells[c].Style.Alignment = DataGridViewContentAlignment.MiddleRight;

						if (CompanyList.Count == 1) continue;

						switch (IndicatorsList[r].OrderBy)
						{
							case Ordering.Ascending:
								var maxOrder = CompanyList.Where(p => !float.IsNaN(p.Values[r - 1]));

								if (maxOrder.Any())
								{
									var max = maxOrder.Max(x => x.Values[r - 1]);

									if (CompanyList[c - 1].Values[r - 1] == max)
									{
										dataGridView.Rows[r].Cells[c].Style.Font = new Font(dataGridView.Font, FontStyle.Bold);
									}
								}
								break;

							case Ordering.Descending:
								var minOrder = CompanyList.Where(p => !float.IsNaN(p.Values[r - 1]));

								if (minOrder.Any())
								{
									var min = minOrder.Min(x => x.Values[r - 1]);

									if (CompanyList[c - 1].Values[r - 1] == min)
									{
										dataGridView.Rows[r].Cells[c].Style.Font = new Font(dataGridView.Font, FontStyle.Bold);
									}
								}
								break;

							case Ordering.None:
								if (dataGridView.Rows[r].Cells[c].Value.ToString() == "Ativo")
								{
									dataGridView.Rows[r].Cells[c].Style.Font = new Font(dataGridView.Font, FontStyle.Bold);
								}
								break;
						}

						if (IndicatorsList[r].MinValue != float.MinValue && CompanyList[c - 1].Values[r - 1] < IndicatorsList[r].MinValue)
						{
							dataGridView.Rows[r].Cells[c].Style.ForeColor = Color.Red;
						}

						if (IndicatorsList[r].MaxValue != float.MaxValue && CompanyList[c - 1].Values[r - 1] > IndicatorsList[r].MaxValue)
						{
							dataGridView.Rows[r].Cells[c].Style.ForeColor = Color.Red;
						}
					}
				}
			}

			foreach (DataGridViewColumn column in dataGridView.Columns)
			{
				column.SortMode = DataGridViewColumnSortMode.NotSortable;
				column.MinimumWidth = 100;
				column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
			}
		}



		private Company GetNewCompany()
		{
			var newCompany = new Company() { Values = new float[IndicatorsList.Count - 1] };
			for (int i = 0; i < newCompany.Values.Length; i++)
			{
				newCompany.Values[i] = float.NaN;
			}
			return newCompany;
		}
	}
}
