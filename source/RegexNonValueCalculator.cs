﻿using System.Text.RegularExpressions;

namespace StockScreener
{
	class RegexNonValueCalculator : IValueCalculator
	{
		private string _regex;
		private int _groupIndex;

		public RegexNonValueCalculator(string regex, int groupIndex = 2)
		{
			_regex = regex;
			_groupIndex = groupIndex;
		}

		public float CalculateValue(Company company)
		{
			var stringValue = Regex.Match(company.URLData.ToString(), _regex).Groups[_groupIndex].Value;

			return stringValue == "Ativo" ? 1 : 0;
		}
	}
}
