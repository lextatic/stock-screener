﻿using System.Text.RegularExpressions;

namespace StockScreener
{
	class RegexExtractValueCalculator : IValueCalculator
	{
		private string _regex;
		private int _groupIndex;
		private float _multiplier;

		public RegexExtractValueCalculator(string regex, int groupIndex = 3, float multiplier = 1)
		{
			_regex = regex;
			_groupIndex = groupIndex;
			_multiplier = multiplier;
		}

		public float CalculateValue(Company company)
		{
			var stringValue = Regex.Match(company.URLData.ToString(), _regex).Groups[_groupIndex].Value;
			float floatValue = float.NaN;
			if (float.TryParse(stringValue, out floatValue))
			{
				return floatValue * _multiplier;
			}
			return float.NaN;
		}
	}
}
