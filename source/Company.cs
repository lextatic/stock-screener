﻿namespace StockScreener
{
	public struct Company
	{
		public string Name { get; set; }

		public string URLData { get; set; }

		public float[] Values { get; set; }
	}
}
