﻿namespace StockScreener
{
	class DummyValueCalculator : IValueCalculator
	{
		public float CalculateValue(Company company)
		{
			return 1;
		}
	}
}
