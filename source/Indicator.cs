﻿namespace StockScreener
{
	enum IndicatorType
	{
		AllRounders,
		Lucratividade,
		Crescimento,
		Endividamento,
		Governanca,
		Outros
	}

	enum FormatType
	{
		None,
		TwoDecimalPlaces,
		Percentage,
		M,
		B,
		Ativo
	}

	enum Ordering
	{
		None,
		Ascending,
		Descending
	}

	class Indicator
	{
		public const string RegexIndicatorsPart1 = @"(>\s*";
		public const string RegexFloatIndicatorsPart3_2 = @"\s*<[\s\S]*?)((?=-?\d+(\.\d{1,3})*,?\d+%*<)(-?\d+(\.\d{1,3})*,?\d+)|(?=-%*\s*<)(-))";
		public const string RegexPL = @"P\/L";
		public const string RegexPVP = @"P\/VP";
		public const string RegexDivLiqEbit = @"D&#xED;v. l&#xED;quida/EBIT";

		public const string RegexDY = @"Dividend Yield";
		public const string RegexROE = @"ROE";
		public const string RegexMLiquida = @"M. L&#xED;quida";
		public const string RegexCAGRLucro5a = @"CAGR Lucros 5 anos";

		public const string regexMainIndicatorsPart3 = @"\s*<[\s\S]*?)(?=-?\d*,?\d+%*<)(-?\d*,?\d+)";
		public const string RegexPrice = @"Valor atual";
		public const string RegexDY12M = @"Últimos 12 meses";

		public const string regexTableIndicatorsPart3 = @"[\s\S]*?)((?=-?\d*.?\d*,?\d+\sM\s<)(-?\d*.?\d*,?\d+)|(?=\s*-\s*<\/span>)(-))";
		public const string RegexLucroLiq = @"Lucro L&#xED;quido";
		public const string RegexDivBruta = @"D&#xED;vida Bruta";
		public const string RegexEBITDA = @"EBITDA -";
		public const string RegexReceitaLiq = @"Receita L&#xED;quida";

		public const string RegexNumeroPapeis = @"[\d{3}(\.\d{3})]*(?=\s*<\/strong>\s*<\/div>\s*<\/div>\s*<\/div>\s*<div class=""info"">\s*<div title=""Segmento de listagem)";
		public const string RegexValorMercadoPart3 = @"[\s\S]*?)(?=\d+(\.\d{1,3})+)(\d+(\.\d{1,3})+)";
		public const string RegexValorMercadoPart2 = @"Valor de mercado<";

		public const string RegexRecompra = @"(>\s*PROGRAMA DE RECOMPRA\s*<[\s\S]*?)((?=Ativo)(Ativo)|(?=NÃO)(NÃO)|(?=Finalizado)(Finalizado))";

		public string Name;

		public IValueCalculator ValueCalculator;

		public IndicatorType IndicatorType = IndicatorType.Outros;

		public FormatType FormatType = FormatType.None;

		public Ordering OrderBy = Ordering.None;

		public float MinValue = float.MinValue;

		public float MaxValue = float.MaxValue;

		public float CalculateValue(Company company)
		{
			return ValueCalculator.CalculateValue(company);
		}
	}
}
