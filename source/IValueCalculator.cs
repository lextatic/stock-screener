﻿namespace StockScreener
{
	interface IValueCalculator
	{
		float CalculateValue(Company company);
	}
}
