﻿namespace StockScreener
{
	class DivisionValueCalculator : IValueCalculator
	{
		private int _numerator;
		private int _denominator;
		private float _numeratorMultiplier;
		private float _denominatorMultiplier;

		public DivisionValueCalculator(int numerator, int denominator, float numeratorMultiplier = 1, float denominatorMultiplier = 1)
		{
			_numerator = numerator;
			_denominator = denominator;
			_numeratorMultiplier = numeratorMultiplier;
			_denominatorMultiplier = denominatorMultiplier;
		}

		public float CalculateValue(Company company)
		{
			if (_denominator == 0) return 0;
			return (company.Values[_numerator] * _numeratorMultiplier) / (company.Values[_denominator] * _denominatorMultiplier);
		}
	}
}
