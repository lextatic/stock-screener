# Introdução
Programa simples que auxilia a filtrar ações para investimento em um processo chamado de Screening.

O programa extrai os dados do site [StatisInvest](https://statusinvest.com.br/) que também possui um comparativo de ações na área logada. Na verdade, exceto talvez por um fator de praticidade, não existe muito motivo para utilizar este programa ao invés da própria comparação do site. A grande vantagem deste projeto é que me permite adicionar dados que o StatusInvest não utiliza no comparador dele, como por exemplo, se existe um programa de recompra ativo ou não.

Programadores podem clonar o projeto e expandi-lo ainda mais caso desejável.

# Download

- [Versão 1.0](https://gitlab.com/lextatic/stock-screener/-/blob/master/binaries/StockScreener.exe)

Basta baixar o arquivo e executar.

# Como usar

Para utilizar o programa é só preencher os campos em branco com os Tickers das empresas que deseja comparar. É possível editar e substituir um ticker que já esteja preenchido.

Para adicionar mais empresas basta utilizar a coluna em branco à direita.

Para limpar a lista basta clicar no botão Limpar na parte inferior do programa.

Os indicadores selecionados são baseados nas recomendações de indicadores do [O Primo Rico](https://www.youtube.com/user/thigas).

- **All Rounders**
	- **P/L:** Relação entre o preço e a lucratividade da empresa.
	- **P/VP:** Qual a relação do valor sendo negociado e o total de patrimônio da empresa.
- **Lucratividade**
	- **ROE:** Análise do retorno direto sobre o captal do investidor.
	- **Margem Líquida:** Para saber se a empresa está tendo margem de lucro saudável.
- **Crescimento**
	- **CAGR Lucro 5 Anos:** Para verificar o crescimento dos últimos anos.
	- **PEG Ratio: Para entender qual a relação de Lucro e Crescimento da empresa.
- **Endividamento**
	- **Dívida Líquida / EBIT:** Para saber se o endividamento da empresa está saldável.
- **Governança**
	- **Programa de Recompras:** Adicionei este por conta própria, útil para saber quando a empresa está recomprando as próprias ações.
- **Outros**
	- **Valor de Mercado:** Útil para ter uma noção do tamanho da empresa.

Os melhores valores serão indicados sempre em negrito, a seta no nome indica se o indicador é melhor quando crescente ou decrescente.

Valores em vermelho apresentam indicadores que saíram de uma "faixa segura" de valor e devem ser estudados com mais cuidado. Eles podem significar alguma distorção nos dados e alguns cuidados extras para o qual o investidor deve prestar atenção.

# Nota final

Como os dados são extraídos do site do StatusInvest, existe uma grande possibilidade de que qualquer alteração no site quebre os Regex utilizados para a extração dos dados e consequentemente o programa pare de funcionar ou passe a pegar valores errados. Nesse caso você sempre poderá utilizar a própria ferramenta de comparação do StatisInvest, nela é garatido que os valores estarão sempre atualizados e funcionando corretamente.